package testbelajarkotlin.com.belajarkotlin.activity

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.*
import testbelajarkotlin.com.belajarkotlin.R
import testbelajarkotlin.com.belajarkotlin.model.Item

class DetailActivity : AppCompatActivity() , AnkoLogger{


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DetailActivityUi().setContentView(this)

        initData()

    }

    private fun initData() {

        val itemClub = intent.getParcelableExtra<Item>("items")
        val clubName = txtClubName
        info { itemClub }
        debug {itemClub.name }
        val clubImages = ivClubImg
        val clubDesc = txtClubDesc

        clubName.text = itemClub.name
        Glide.with(this)
                .load(itemClub.image)
                .into(clubImages)
        clubDesc.text = itemClub.desc
    }


    class DetailActivityUi: AnkoComponent<DetailActivity> {
        override fun createView(ui: AnkoContext<DetailActivity>): View = with (ui) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            verticalLayout{
                padding = dip(16)

                imageView{
                    id = R.id.ivClubImg
                }.lparams(400, 400){
                    bottomMargin = dip(20)
                    padding = dip(8)
                    gravity = Gravity.CENTER
                }
                textView{
                    id = R.id.txtClubName
                    textSize = 18f
                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                    typeface = Typeface.DEFAULT_BOLD
                }.lparams{
                    width = wrapContent
                    height = wrapContent
                    setMargins(dip(10), 0, dip(10), dip(20))
                    gravity = Gravity.CENTER
                }


                textView{
                    id = R.id.txtClubDesc
                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                }.lparams{
                    width = wrapContent
                    height = wrapContent
                    gravity = Gravity.CENTER
                }
            }
        }

    }
}
