package testbelajarkotlin.com.belajarkotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import org.jetbrains.anko.*
import kotlinx.android.synthetic.main.item_list.view.*
import testbelajarkotlin.com.belajarkotlin.activity.DetailActivity
import testbelajarkotlin.com.belajarkotlin.R
import testbelajarkotlin.com.belajarkotlin.model.Item

/**
 * Created              : Rahman on 04/09/2018.
 * Date Created         : 04/09/2018 / 0:21.
 * ===================================================
 * Package              : testbelajarkotlin.com.belajarkotlin.
 * Project Name         : BelajarKotlin.
 * Copyright            : Copyright @ 2018 Sudeska.
 */
class RecyclerViewAdapter (var items: List<Item>)
    : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                ClubListUi().createView(
                        AnkoContext.create(parent.context, parent)
                )
        )

    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
       holder.bindItem(items[position])

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        //val name.itemView.name
        //val images.itemView.image
        //val itemView.name.text

        fun bindItem(items: Item) {
            itemView.tvName.text = items.name
            Glide.with(itemView.context).load(items.image).into(itemView.ivImage)
            itemView.setOnClickListener {
                itemView.context.startActivity<DetailActivity>("items" to items)
                //listener(items)
            }

        }

    }


    class ClubListUi : AnkoComponent<ViewGroup>{
        override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            linearLayout {
                padding = dip (10)
                orientation = LinearLayout.HORIZONTAL
                imageView {
                    id = R.id.ivImage
                    setImageResource(R.drawable.img_barca)

                }.lparams {
                    width = dip (50)
                    height = dip (50)

                }

                textView {
                    id = R.id.tvName
                    text = "Barcelona FC"
                }.lparams{
                    width = wrapContent
                    height = wrapContent
                    gravity = Gravity.CENTER_VERTICAL
                    margin = dip(10)
                }



            }
        }

    }


}

