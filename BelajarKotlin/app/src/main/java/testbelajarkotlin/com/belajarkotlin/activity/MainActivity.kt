package testbelajarkotlin.com.belajarkotlin.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import testbelajarkotlin.com.belajarkotlin.R
import testbelajarkotlin.com.belajarkotlin.adapter.RecyclerViewAdapter
import testbelajarkotlin.com.belajarkotlin.model.Item

class MainActivity : AppCompatActivity() {

    private val items: MutableList<Item> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUI().setContentView(this)
        initData()
        val list = club_list
        list.layoutManager = LinearLayoutManager(this)

        val mAdapter = RecyclerViewAdapter(items)
        list.adapter = mAdapter


    }

    private fun initData() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val name = resources.getStringArray(R.array.club_name)
        val image = resources.obtainTypedArray(R.array.club_image)
        val desc = resources.getStringArray(R.array.club_desc)
        items.clear()
        for (i in name.indices) {
            items.add(Item(name[i],
                    image.getResourceId(i, 0), desc[i]))
        }

        //Recycle the typed array
        image.recycle()

    }


    class MainActivityUI : AnkoComponent<MainActivity>{
        override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
            relativeLayout {
                recyclerView{
                    id = R.id.club_list
                }.lparams(width = matchParent, height = wrapContent)
            }
        }
    }
}







